package com.example.kafka1.consumer;

import com.example.kafka1.producer.Acesso;
import com.opencsv.CSVWriter;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;

import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.nio.file.Files;
import java.nio.file.Paths;

@Component
public class AcessoConsumer {

    @KafkaListener(topics = "spec3-julio-felipe-1", groupId = "gazorpazorp")
    public void receber(@Payload Acesso acesso) throws IOException {
        String[] log = {("Recebi um acesso " + acesso.isAcesso() + " do cliente " +
                acesso.getCliente() + " na porta " + acesso.getPorta())};
        System.out.println(log);

        FileWriter fw = new FileWriter("empresasaceitas.csv", true);
        CSVWriter csvWriter = new CSVWriter(fw);

        csvWriter.writeNext(log);

        csvWriter.flush();
        fw.close();

    }
}
